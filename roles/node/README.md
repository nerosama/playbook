# Ansible Role: Node

Installs [Node.js](https://nodejs.org), [NPM](https://www.npmjs.com) package manager.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [geerlingguy.nodejs](https://github.com/geerlingguy/ansible-role-nodejs)

## References

This role uses the following resouces as references:

TBD
