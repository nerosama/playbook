# Ansible Role: Xfce

Configures Xfce desktop environment.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [gantsign.xdesktop](https://github.com/gantsign/ansible-role-xdesktop)
- [ferrarimarco.xfce](https://github.com/ferrarimarco/ansible-role-xfce)
- [webofmars.xfce4-desktop](https://github.com/webofmars/ansible-xfce4-desktop)

## References

This role uses the following resouces as references:

- [Useful Thunar Custom Actions](http://duncanlock.net/blog/2013/06/28/useful-thunar-custom-actions/)
