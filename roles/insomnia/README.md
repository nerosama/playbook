# Ansible Role: Insomnia

Installs and configures [Insomnia](https://insomnia.rest/).

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

TBD

## References

This role uses the following resouces as references:

TBD
