# Ansible Role: Devilbox

Installs and configures [Devilbox](https://github.com/cytopia/devilbox).

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

TBD

## References

This role uses the following resouces as references:

- https://github.com/cytopia/devilbox
- https://devilbox.readthedocs.io/en/latest/
- https://github.com/louisgab/devilbox-cli
- https://github.com/philosullivan/devilbox-scripts
