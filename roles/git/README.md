# Ansible Role: Git

Installs Git version control software.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [geerlingguy.git](https://github.com/geerlingguy/ansible-role-git)
- [kosssi.gitconfig](https://github.com/kosssi/ansible-role-gitconfig)
- [laggyluke.add-ssh-keys-from-github](https://github.com/laggyluke/ansible-role-add-ssh-keys-from-github)
- [smartlogic.github_keys](https://github.com/smartlogic/ansible-role-github_keys)
- [geerlingguy.github-users](https://github.com/geerlingguy/ansible-role-github-users)
- [pli01.ansible-gitlab-config](https://github.com/pli01/ansible-gitlab-config)
- [gnagano.ansible_bash_easy_github_settings](https://github.com/Gnagano/ansible_bash_easy_github_settings)
- [pylabs.add_ssh_known_hosts](https://github.com/pylabs/ansible-add-ssh-known-hosts)

## References

This role uses the following resouces as references:

TBD
