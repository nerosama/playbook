# Ansible Role: Tor

Installs and configures [Tor Browser Bundle](https://www.torproject.org/).

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [parmegv.ansible-tor-browser-bundle](https://github.com/parmegv/ansible-tor-browser-bundle)

## References

This role uses the following resouces as references:

- https://github.com/micahflee/torbrowser-launcher
