# Ansible Role: Admin

Creates and configures the main user account.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [socketwench.users-and-groups](https://github.com/socketwench/ansible-role-users-and-groups)
- [thydel.my-account](https://github.com/thydel/ar-my-account)
- [oefenweb.user](https://github.com/Oefenweb/ansible-user)
- [rickapichairuk.add-admin-user](https://github.com/rickapichairuk/ansible-add-admin-user)

## References

This role uses the following resouces as references:

TBD
