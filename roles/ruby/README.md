# Ansible Role: Ruby

Installs [Ruby](https://www.ruby-lang.org/en/) and [Ruby Gems](https://rubygems.org/).

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [geerlingguy.ruby](https://github.com/geerlingguy/ansible-role-ruby)
- [MichaelRigart.brightbox-ruby](https://github.com/michaelrigart/ansible-role-brightbox-ruby)

## References

This role uses the following resouces as references:

TBD
