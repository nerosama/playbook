# Ansible Role: Sublime

Installs [Sublime Text](https://www.sublimetext.com/) text editor.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [chaosmail.sublime-text](https://github.com/chaosmail/ansible-roles-sublime-text)
- [theNewFlesh.sublime](https://github.com/theNewFlesh/ansible-role-sublime)
- [theNewFlesh.sublime-config](https://github.com/theNewFlesh/ansible-role-sublime-config)
- [fagia.sublimetext](https://github.com/fagia/sublimetext)

## References

This role uses the following resouces as references:

TBD
