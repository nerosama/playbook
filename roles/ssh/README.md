# Ansible Role: Ssh

Generates SSH keys & configuration.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [ysz.ssh-with-keys](https://github.com/ysz/ansible-role-ssh-with-keys)
- [oefenweb.ssh_keys](https://github.com/Oefenweb/ansible-ssh-keys)
- [wunzeco.ssh-config](https://github.com/wunzeco/ansible-ssh-config)
- [viasite-ansible.ssh-keys](https://github.com/viasite-ansible/ansible-role-ssh-keys)
- [net2grid.ssh_keypair](https://github.com/net2grid/ansible-ssh_keypair)

## References

This role uses the following resouces as references:

- https://gist.github.com/jexchan/2351996
- https://coderwall.com/p/7smjkq/multiple-ssh-keys-for-different-accounts-on-github-or-gitlab
- https://blog.wizardsoftheweb.pro/
- https://stackoverflow.com/questions/29392369/ansible-ssh-private-key-in-source-control
