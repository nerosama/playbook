# Ansible Role: Bash

Sets up the bash shell.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [salamachinas.shell-users](https://github.com/salamachinas/ansible-shell-users)
- [reallyenglish.shells](https://github.com/reallyenglish/ansible-role-shells)
- [mm0.bash](https://github.com/mm0/ansible-role-bash)
- [blazingbarons.nicer-bash-prompt](https://github.com/blazingbarons/ansible-role-nicer-bash-prompt)
- [mrlesmithjr.bashrc](https://github.com/mrlesmithjr/ansible-bashrc)
- [spk83.bash-profile](https://github.com/spk83/ansible-bash-profile)
- [carlba.bash_aliases](https://github.com/carlba/ansible-role-bash_aliases)
- [andrewrothstein.bash](https://github.com/andrewrothstein/ansible-bash)
- [dalee.bootstrap](https://github.com/Dalee/bootstrap)
- [shelleg.shell-goodies](https://github.com/shelleg/ansible-role-shell-goodies)
- [hadrienpatte.my_shell](https://github.com/HadrienPatte/ansible-role-my-shell)
- [oefenweb.bash](https://github.com/Oefenweb/ansible-bash)

## References

This role uses the following resouces as references:

TBD
