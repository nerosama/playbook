# Ansible Role: Common

Base role to install some common packages and utilites.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [bbatsche.Base](https://github.com/bbatsche/Ansible-Common-Role)
- [robertdebock.bootstrap](https://github.com/robertdebock/ansible-role-bootstrap)
- [robertdebock.common](https://github.com/robertdebock/ansible-role-common)
- [webbylab.common](https://github.com/WebbyLab/ansible-role-common)
- [mrlesmithjr.bootstrap](https://github.com/mrlesmithjr/ansible-bootstrap)
- [mrlesmithjr.base](https://github.com/mrlesmithjr/ansible-base)

## References

This role uses the following resouces as references:

TBD
