# Ansible Role: Golang

Installs [Go language](https://golang.org/).

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [gantsign.golang](https://github.com/gantsign/ansible-role-golang)

## References

This role uses the following resouces as references:

TBD
