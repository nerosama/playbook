# Ansible Role: Visual Studio Code

Installs [Visual Studio Code](https://code.visualstudio.com/) text editor.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [gantsign.visual-studio-code](https://github.com/gantsign/ansible-role-visual-studio-code)

## References

This role uses the following resouces as references:

TBD
