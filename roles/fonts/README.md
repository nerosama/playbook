# Ansible Role: Fonts

Installs and configures fonts.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [oefenweb.fonts](https://github.com/Oefenweb/ansible-fonts)
- [ontic.fonts](https://github.com/ontic/ansible-role-fonts)

## References

This role uses the following resouces as references:

TBD
