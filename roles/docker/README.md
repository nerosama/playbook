# Ansible Role: Docker

Installs and configures [Docker](https://www.docker.com).

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [geerlingguy.docker](https://github.com/geerlingguy/ansible-role-docker)
- [angstwad.docker_ubuntu](https://github.com/angstwad/docker.ubuntu)

## References

This role uses the following resouces as references:

- [Docker Indicator](https://github.com/yktoo/indicator-docker)
