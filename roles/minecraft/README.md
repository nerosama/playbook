# Ansible Role: Minecraft

Installs and configures [Minecraft](https://minecraft.net/).

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [devops-coop.minecraft](https://github.com/devops-coop/ansible-minecraft)
- [meatballhat.minecraft](https://github.com/meatballhat/ansible-role-minecraft)
- [mamercad.minecraft](https://github.com/mamercad/ansible-minecraft)
- [ilbonzo.minecraft](https://github.com/ilbonzo/ansible-minecraft)
- [jyunderwood.mapcrafter](https://github.com/jyunderwood/ansible-mapcrafter)

## References

This role uses the following resouces as references:

TBD
