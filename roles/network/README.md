# Ansible Role: Network

Configures Virtual Private Network.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [oasis-roles.nmcli_add_addrs](https://github.com/oasis-roles/nmcli_add_addrs)

## References

This role uses the following resouces as references:

- [Import VPN Config Files to NetworkManager from Command Line](https://unix.stackexchange.com/questions/140163/import-vpn-config-files-to-networkmanager-from-command-line)
- [VPN Autoconnect Command Line](http://blog.deadlypenguin.com/blog/2017/04/24/vpn-auto-connect-command-line/)
- [Import OpenVPN File with NetworkManager](https://www.cyberciti.biz/faq/linux-import-openvpn-ovpn-file-with-networkmanager-commandline/)
- [Use nmcli to Modify VPN Password](https://unix.stackexchange.com/questions/335999/use-nmcli-to-modify-vpn-password)
- [Randomize your MAC address using NetworkManager](https://fedoramagazine.org/randomize-mac-address-nm/)
- [Set custom DNS servers on Linux with Network Manager or resolv.conf](https://pchelp.ricmedia.com/set-custom-dns-servers-linux-network-manager-resolv-conf/)
- [How to Configure and Manage Network Connections Using ‘nmcli’ Tool](https://www.tecmint.com/configure-network-connections-using-nmcli-tool-in-linux/)
