# Ansible Role: Dropbox

Installs and configures [Dropbox](https://www.dropbox.com/).

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [oefenweb.dropbox](https://github.com/Oefenweb/ansible-dropbox)
- [wtanaka.dropbox](https://github.com/wtanaka/ansible-role-dropbox)
- [patrick-hill.dropbox](https://github.com/patrick-hill/ansible-role-dropbox)
- [jdauphant.dropbox](https://github.com/jdauphant/ansible-role-dropbox)

## References

This role uses the following resouces as references:

TBD
