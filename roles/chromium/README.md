# Ansible Role: Chromium

Installs and configures the [Google Chromium browser](https://www.chromium.org/Home) web browser.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [cmprescott.chrome](https://github.com/cmprescott/ansible-role-chrome)
- [Oefenweb.chrome](https://github.com/Oefenweb/ansible-chrome)
- [escalate.google-chrome](https://github.com/escalate/ansible-google-chrome)

## References

This role uses the following resouces as references:

TBD
