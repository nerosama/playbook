# tasks file for chromium
---

# Install Chromium
- name: Ensure Chromium is installed
  become: yes
  apt:
    name: "{{ chromium_packages | list }}"
    state: present
    update_cache: yes
    cache_valid_time: 3600
  retries: 5
  delay: 2
  register: chromium_pkg_installed
  until: chromium_pkg_installed is succeeded
  when: chromium_packages | length

# Disable Keyring
# https://ubuntuforums.org/showthread.php?t=2377036
- name: Disable Chromium keyring
  become: yes
  lineinfile:
    path: /etc/chromium-browser/default
    line: CHROMIUM_FLAGS="--password-store=basic"
    regexp: "^CHROMIUM_FLAGS="

# Set Chromium Preferences
- name: Delete Chromium preferences
  file:
    path: "{{ chromium_preferences }}"
    state: absent

- name: Create Chromium preferences file
  copy:
    dest: "{{ chromium_preferences }}"
    content: "{{ chromium_preferences_json | to_nice_json }}"
  when: chromium_preferences_json

# Set Chromium Policies
- name: Create Chromium policies directory for all users
  become: yes
  file:
    path: "{{ chromium_policies_dir }}/{{ item }}"
    state: directory
    recurse: true
  with_items:
    - managed
    - recommended

- name: Set Chromium policies
  become: yes
  copy:
    dest: "{{ chromium_policies_dir }}/{{ item.key }}/{{ chromium_policies_filename }}"
    mode: 0644
    content: "{{ item.value|to_nice_json }}"
  when: item.value|length > 0
  with_dict:
    managed: "{{ chromium_policies_managed }}"
    recommended: "{{ chromium_policies_recommended }}"

- name: Delete Chromium policy file if no policies are defined
  become: yes
  file:
    path: "{{ chromium_policies_dir }}/{{ item.key }}/{{ chromium_policies_filename }}"
    state: absent
  when: item.value|length == 0
  with_dict:
    managed: "{{ chromium_policies_managed }}"
    recommended: "{{ chromium_policies_recommended }}"

# Install Chromium Extensions
# See https://developer.chrome.com/extensions/external_extensions#preferences
- name: Create Chromium extensions directory for all users
  become: yes
  file:
    path: "{{ chromium_extensions_dir }}"
    state: directory
    mode: 0755
  check_mode: no
  when: chromium_extensions

- name: Create Chromium JSON file to install extensions for all users
  become: yes
  template:
    src: chrome-extensions.json.j2
    dest: "{{ chromium_extensions_dir }}/{{ item }}.json"
    owner: root
    group: root
    mode: 0644
  with_items: "{{ chromium_extensions }}"
  when: chromium_extensions
