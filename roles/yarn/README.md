# Ansible Role: Yarn

Installs the [Yarn](https://yarnpkg.com) package manager.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [ocha.yarn](https://github.com/ocha/ansible-role-yarn)

## References

This role uses the following resouces as references:

TBD
