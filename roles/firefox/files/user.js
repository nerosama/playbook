user_pref("_user.js.parrot", "START: Oh yes, the Norwegian Blue... what's wrong with it?");

user_pref("app.shield.optoutstudies.enabled", false);
user_pref("browser.discovery.enabled", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.highlights", false);
user_pref("browser.newtabpage.activity-stream.feeds.snippets", false);
user_pref("browser.newtabpage.activity-stream.feeds.topsites", false);
user_pref("browser.newtabpage.activity-stream.prerender", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeBookmarks", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeDownloads", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includeVisited", false);
user_pref("browser.privatebrowsing.autostart", true);
user_pref("browser.tabs.warnOnClose", false);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[\"canvasblocker_kkapsner_de-browser-action\",\"_74145f27-f039-47ce-a470-a662b129930a_-browser-action\",\"_7fc8ef53-24ec-4205-87a4-1e745953bb0d_-browser-action\",\"jid1-bofifl9vbdl2zq_jetpack-browser-action\",\"_33c93ccc-ceed-47d2-9645-805ea58c8a07_-browser-action\",\"headereditor-amo_addon_firefoxcn_net-browser-action\",\"jid1-mnnxcxisbpnsxq_jetpack-browser-action\",\"poop_cm_org-browser-action\",\"_ed2ba263-72d9-420b-8422-37210f522948_-browser-action\",\"site-bleacher_vukmirovic_org-browser-action\",\"skipredirect_sblask-browser-action\",\"_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action\",\"_529b261b-df0b-4e3b-bf42-07b462da0ee8_-browser-action\",\"_2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c_-browser-action\",\"_d07ccf11-c0cd-4938-a265-2a4d6ad01189_-browser-action\"],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"downloads-button\",\"library-button\",\"sidebar-button\",\"_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action\",\"support_lastpass_com-browser-action\",\"ublock0_raymondhill_net-browser-action\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"personal-bookmarks\"]},\"seen\":[\"developer-button\",\"_446900e4-71c2-419f-a6a7-df9c091e268b_-browser-action\",\"canvasblocker_kkapsner_de-browser-action\",\"_74145f27-f039-47ce-a470-a662b129930a_-browser-action\",\"_7fc8ef53-24ec-4205-87a4-1e745953bb0d_-browser-action\",\"jid1-bofifl9vbdl2zq_jetpack-browser-action\",\"_33c93ccc-ceed-47d2-9645-805ea58c8a07_-browser-action\",\"headereditor-amo_addon_firefoxcn_net-browser-action\",\"support_lastpass_com-browser-action\",\"jid1-mnnxcxisbpnsxq_jetpack-browser-action\",\"poop_cm_org-browser-action\",\"_ed2ba263-72d9-420b-8422-37210f522948_-browser-action\",\"_2e5ff8c8-32fe-46d0-9fc8-6b8986621f3c_-browser-action\",\"site-bleacher_vukmirovic_org-browser-action\",\"skipredirect_sblask-browser-action\",\"_7a7a4a92-a2a0-41d1-9fd7-1e92480d612d_-browser-action\",\"ublock0_raymondhill_net-browser-action\",\"_529b261b-df0b-4e3b-bf42-07b462da0ee8_-browser-action\",\"_d07ccf11-c0cd-4938-a265-2a4d6ad01189_-browser-action\"],\"dirtyAreaCache\":[\"nav-bar\",\"toolbar-menubar\",\"TabsToolbar\",\"PersonalToolbar\",\"widget-overflow-fixed-list\"],\"currentVersion\":15,\"newElementCount\":3}");
user_pref("browser.urlbar.placeholderName", "DuckDuckGo");
user_pref("browser.urlbar.suggest.bookmark", false);
user_pref("browser.urlbar.suggest.history", false);
user_pref("browser.urlbar.suggest.openpage", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("media.autoplay.default", 1);
user_pref("network.cookie.lifetimePolicy", 2);
user_pref("network.proxy.socks", "10.8.0.1");
user_pref("network.proxy.socks_port", 1080);
user_pref("network.proxy.socks_remote_dns", true);
user_pref("network.proxy.type", 1);
user_pref("permissions.default.camera", 2);
user_pref("permissions.default.desktop-notification", 2);
user_pref("permissions.default.geo", 2);
user_pref("permissions.default.microphone", 2);
user_pref("privacy.firstparty.isolate", true);

user_pref("_user.js.parrot", "SUCCESS: No no he's not dead, he's, he's restin'!");


