# Ansible Role: Firefox

Installs and configures the [Firefox](https://www.mozilla.org/firefox/) web browser.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [maxbachmann.firefox](https://github.com/maxbachmann/role-firefox)
- [unrblt.firefox](https://github.com/unrblt/ansible-role-firefox)
- [conorsch.firefox](https://github.com/conorsch/ansible-role-firefox)
- [arknoll.firefox](https://github.com/arknoll/ansible-role-firefox)
- [juju4.ansible-firefox-config](https://github.com/juju4/ansible-firefox-config)
- [tschifftner.firefox](https://github.com/tschifftner/ansible-role-firefox)

## References

This role uses the following resouces as references:

- [pyllyukko user.js](https://github.com/pyllyukko/user.js)
- [ghacks user.js](https://github.com/ghacksuserjs/ghacks-user.js)
- [Firefox Enterprise Deployment](https://developer.mozilla.org/en-US/Firefox/Enterprise_deployment)
- [Sideloading Add-ons](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Alternative_distribution_options/Sideloading_add-ons)
- [Ansible: Install Firefox Extensions](http://www.fvue.nl/wiki/Ansible:_Install_Firefox_extension)
- [Install Firefox Add-ons from command line](http://askubuntu.com/questions/73474/how-to-install-firefox-addon-from-command-line-in-scripts)
- [Installing extensions](http://kb.mozillazine.org/Installing_extensions)
- [Linux sandboxing improvements](https://www.morbo.org/2018/05/linux-sandboxing-improvements-in_10.html)
- [Major changes coming in Firefox](https://mike.kaply.com/2013/05/13/more-major-changes-coming-in-firefox-21/)
- [Customizing Firefox default profiles](https://mike.kaply.com/2012/03/30/customizing-firefox-default-profiles/)
- [Everything Firefox](https://12bytes.org/articles/tech/firefox)
