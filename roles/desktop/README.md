# Ansible Role: Desktop

Installs and configures a variety of Ubuntu/Xfce desktop packages.

## Requirements

No requirements.

## Dependencies

No dependencies.

## Credits

This role takes inspiration from the following Ansible roles:

- [gabethecabbage.traditional-desktop](https://github.com/gabethecabbage/ansible-role-traditional-desktop)
- [danieljaouen.base-ubuntu](https://github.com/danieljaouen/ansible-base-ubuntu)
- [ansibl3.ubuntu_laptop](https://github.com/ansibl3/ubuntu_laptop)

## References

This role uses the following resouces as references:

TBD
