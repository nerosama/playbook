Provision Resources
===================

A curated list of resources to help with provisioning a
Chromebook to an GalliumOS development environment with Ansible.

### Provisioning

- [ubuntu-xenial-provisioning](https://github.com/koooge/ubuntu-xenial-provisioning)
- [Perfect Ubuntu](https://github.com/andreystarkov/perfect-ubuntu): on the way to perfect open source dev enviroment
- https://github.com/cochran-at-niche/home

### Lamp Scripts

- [TeddySun Lamp](https://github.com/teddysun/lamp)
- https://github.com/linuxeye/lamp
- https://github.com/Marko-M/lamp-vhost-manager
- https://github.com/lucien144/lemp-stack
- [Automated Lamp Installation Script](https://github.com/arbabnazar/Automated-LAMP-Installation)

### Network Tools

- [MxToolBox: Network Tools](https://mxtoolbox.com/NetworkTools.aspx)

### Vue

- [VueJS Styleguide](https://github.com/jackbarham/vuejs-style-guide)

### Other Dev

- [Marketing for Engineers](https://github.com/LisaDziuba/Marketing-for-Engineers)

Dotfiles
--------

- [Epitron Scripts](https://github.com/epitron/scripts)
- [DaviOSomething](https://github.com/davidosomething/dotfiles)
- [OSX Dev Setup](https://github.com/donnemartin/dev-setup)
- [Mr. Zool](https://github.com/mrzool/dotfiles)
- [Foggalong](https://github.com/Foggalong/.files)
- [Dotfiles XFCE](https://github.com/sergeylukin/dotfiles-xfce)
- [joelbrewster](https://github.com/joelbrewster/dotfiles)
- [jpmurray](https://github.com/jpmurray/dotfiles)
- [lewagon](https://github.com/lewagon/dotfiles)
- [Rawnly](https://github.com/Rawnly/dot-files)
- [avindra](https://github.com/avindra/dotfiles)
- [brycekbargar](https://github.com/brycekbargar/dotfiles)
- [jcarbo](https://github.com/jcarbo/dotfiles)
- [T4ng10r](https://github.com/T4ng10r/dotfiles)
- [ljmf00](https://github.com/ljmf00/dotfiles)
- [kierantbrowne](https://github.com/kierantbrowne/dotfiles)
- [robinandeer](https://github.com/robinandeer/dotfiles)
- [herrbischoff](https://github.com/herrbischoff/dotfiles)
- [TrueFurby](https://github.com/TrueFurby/dotfiles)
- [Avivace](https://github.com/avivace/dotfiles)
- [dikiaap](https://github.com/dikiaap/dotfiles)
- [epavletic](https://github.com/epavletic/dotfiles)
- [MilanAryal](https://github.com/MilanAryal/config)
- [Shagabutdinov](https://github.com/shagabutdinov/dotfiles)
- [Meitoncz](https://github.com/Meitoncz/dotfiles)
- [jgkim](https://github.com/jgkim/ansible-role-dotfiles)
- [issmirnov](https://github.com/issmirnov/ansible-role-dotfiles)
- [swasher](https://github.com/swasher/ansible_dotfiles)
- [benigls](https://github.com/benigls/dotfiles)
- [dirn](https://github.com/dirn/ansible-dotfiles)
- [serialdoom](https://github.com/serialdoom/ansible-dotfiles)
- [aljoscha](https://github.com/aljoscha/ansible-role-dotfiles)
- [gunzy83](https://github.com/gunzy83/ansible-role-dotfiles)
- [Globidev](https://github.com/Globidev/linux-dotfiles)
- [thezimmee](https://github.com/thezimmee/os-zimmee)
- [927589452](https://github.com/927589452/yadm)
- [wkentaro](https://github.com/wkentaro/dotfiles)
- [sonph](https://github.com/sonph/dotfiles)
- [Alraa Dotfiles](https://github.com/alrra/dotfiles)
- https://github.com/TrueFurby/dotfiles
- https://github.com/njkevlani/xfce-dotFiles
- https://github.com/najibni/Arch-XFCE
- https://github.com/stevenbenner/dotfiles
- https://github.com/sergeylukin/dotfiles-xfce
- https://github.com/gasull/dotfiles-xfce4-terminal
- https://github.com/mamyn0va/dotfiles-home
- https://github.com/kaustubhhiware/dotfiles
- https://github.com/in-in/dotfiles
- https://github.com/ErkanBasar/dotfiles
- https://github.com/SimonDevelop/scripts-auto-install
- https://github.com/wsdjeg/DotFiles
- [OSConfig](https://github.com/manuelbachl/OS-Config)

Misc
----

- [Devilbox](http://devilbox.org/)
- [Laradock](http://laradock.io/)
- [Streisand](https://github.com/jlund/streisand)
- [Ubuntu After Install](https://www.thefanclub.co.za/software-selection)
- [Awesome Linux Software](https://github.com/LewisVo/Awesome-Linux-Software)
- [Material MKDocs Theme](https://github.com/squidfunk/mkdocs-material)
- [Chromebook Privacy Settings Students](https://www.eff.org/deeplinks/2015/11/guide-chromebook-privacy-settings-students)
- [How to Install Virtualmin with Webmin, LAMP, BIND, and PostFix on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-virtualmin-with-webmin-lamp-bind-and-postfix-on-ubuntu-16-04)
- https://github.com/NicolasBernaerts/ubuntu-scripts

## Linux

- https://sandstorm.io/
- https://github.com/pirate/ArchiveBox
- https://github.com/bablosoft/BAS

# Configuration Setup

## Linux Sites

- https://linuxize.com/
- https://linuxconfig.org/
- https://www.ubuntupit.com/
- https://itsfoss.com/

## Privacy Sites

- https://forkdrop.io/table-of-contents
- https://paranoidsbible.tumblr.com/
- https://medium.com/@uzakov/pretty-good-setup-pgs-4d3b58b4341a
- https://medium.com/@Electricsheep56/sheeps-noob-guide-to-monero-gui-in-tails-3-2-d75c4e829c17
- https://www.evonax.com/
- https://github.com/trimstray/the-practical-linux-hardening-guide
- https://github.com/drduh/Debian-Privacy-Server-Guide

## Useful Programs

- https://github.com/bovender/indicator-ip
- https://github.com/gentakojima/emojione-picker-ubuntu
- https://github.com/ynh/BTCIndicator
- https://github.com/yurigorokhov/indicator-vbox
- https://cinnamon-spices.linuxmint.com/
- https://blog.ssdnodes.com/blog/dotfiles/
- https://github.com/FreeTubeApp/FreeTube
- https://github.com/alphaaurigae/ownsec
- https://github.com/xtonousou/xfce4-genmon-scripts
- https://github.com/tunnckoCore
- https://github.com/trimstray/the-book-of-secret-knowledge
- https://github.com/ezaquarii/vpn-at-home

### Themes

#### Base VsCode Themes

- [Super One Dark](https://marketplace.visualstudio.com/items?itemName=seansassenrath.vscode-theme-superonedark)
- [Vulgocode](https://marketplace.visualstudio.com/items?itemName=alexandreramos.vulgocode-color-theme)

## Gitlab
https://github.com/xanzy/go-gitlab/issues/267
https://github.com/alexshin/ssh-key-switcher

https://github.com/leesoh
https://github.com/yegorLitvinov/configs


https://gist.github.com/sangeeths/9467061


curl "https://gitlab.example.com/api/v4/projects"

https://stackoverflow.com/questions/32332045/gitlab-api-add-ssh-key

https://docs.gitlab.com/ee/api/users.html

curl -X POST -F "private_token=${userToken}" -F "title=${sshName}" -F "key=${sshKey}" "${gitServer}/user/keys"

curl -X POST -F "private_token=${GITLAB_TOKEN}" -F "title=$(hostname)" -F "key=$(cat ~/.ssh/id_rsa.pub)" "https://gitlab.com/api/v3/user/keys"
