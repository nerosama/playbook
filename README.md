Desktop Playbook
================
[![License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)](https://opensource.org/licenses/MIT)

Ansible playbook for setting up an Ubuntu desktop & server environment for raipid web development. There are many playbooks like it, but this one is mine.

## Quick Start

To reset GalliumOS, run the following from a crosh shell:

    cd ; curl -Os https://chrx.org/go && sh go -r dev -U yourname -H yourhostname

Then, from GalliumOS session, open the terminal and run:

    bash <(wget -qO - https://bitbucket.org/nerosama/playbook/raw/master/start.sh)

## Installation

1. Ensure Ansible & Git are installed.

    $ sudo apt-add-repository ppa:ansible/ansible -y
    $ sudo apt-get update
    $ sudo apt-get install software-properties-common ansible git python-apt -y

2. Clone this repository to your local drive.

    $ git clone https://bitbucket.org/nerosama/playbook.git

3. Run the Ansible playbook from inside this directory.

    $ ansible-playbook playbook.yml -i inventory -K --vault-id @prompt

4. Enter your SUDO and vault password when prompted.

## Adding and Updating Galaxy roles

From time to time, third party roles need to be added or updated to enable new functionality or fix bugs. To update a role (e.g. `geerlingguy.apache`), find the role's `version` setting inside `requirements.yml`, bump the version to the required or latest version of the role, then run the following command _in the same directory as this README file_:

    $ ansible-galaxy install -r requirements.yml --force

Then commit the updated `requirements.yml` file and the new and updated files within the `roles` directory.

## Creating and Modifying Ansible Vault

By default, this playbook stores important and secret credentials in `vars/vault.yml`. To create an encypted vault, run the following command _in the same directory as this README file_:

    $ ansible-vault create vars/vault.yml

Create a password for your vault and confirm it. This will open a file to open in your editor of choice (defaults to Nano, or whatever is your default editor).

> If you want to use nano over vim, export the EDITOR variable and set it to nano: export EDITOR=nano

To edit the vault file, run the following command _in the same directory as this README file_:

    $ ansible-vault edit vars/vault.yml

Save and quit that file. If you edit the file without Vault, you'll see that the file is encrypted.

Other ansible-vault commands available are seen via:

    $ ansible-vault -h

An example for the encypted values the playbook relies on can be found at `vars/sample.vault.yml`.

## Credits

This playbook draws inspiration from the following projects:

- [Drupal VM](https://github.com/geerlingguy/drupal-vm)
- [Mac Dev Playbook](https://github.com/geerlingguy/mac-dev-playbook)
- [GantSign EnV](https://github.com/gantsign/development-environment)
- [Ansible Ubuntu](https://github.com/lvancrayelynghe/ansible-ubuntu)
- [Roots Trellis](https://github.com/roots/trellis)
- [Laravel Settler](https://github.com/laravel/settler)
- [Ansible Debian](https://github.com/cytopia/ansible-debian)
- [Cloudbox](https://github.com/Cloudbox/Cloudbox)
- [AnsiPress](https://github.com/AnsiPress/AnsiPress)
- [Ansible Linux Laptop](https://github.com/dyindude/ansible-linux-laptop)
- [Ultimate Ubuntu 16.04](https://github.com/erikdubois/Ultimate-Ubuntu-16.04)
- [Ubuntu Post Install Scripts](https://github.com/snwh/ubuntu-post-install)
- [Ansible Local](https://github.com/jphafner/ansible-local)
- [Ansible Local Dev](https://github.com/xeneta/ansible-local-dev)

## License

This project is licensed under the [MIT](https://choosealicense.com/licenses/mit/) open source license.

---
<p align="center">🤖</p>
